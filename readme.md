# Setting Up 

 + Install PHP as you would   

 + Composer 
    1. Manually Download Composer from [this link](https://getcomposer.org/download/) 
    2. Copy the the `composer.phar`  file to /usr/local/bin as `composer`
        ```bash 
        $ mv composer.phar /usr/local/bin/composer 
        # type composer into the terminal to see output
        ``` 
  + Laravel 
    + You can install Laravel manually by running
        ```bash 
        $ composer global require "laravel/installer"  
        ```           
    + Note that here might be some errors in the process... A quick search on the internet will reveal the necessary
    PHP modules for your PHP version such as `php7.2-xml`, `php7.2-mbstring` etc....It depends with the PHP version

  + With that set up, create a new Laravel project
    ```bash 
    $ laravel new blog 
    ```  
  + You might want to run `composer update` after the project creation 
  + Now run `composer install` 
  + Nex launch the Laravel app  by running `php artisan serve` : Should the app complain about missing modules and `vendor/autoload.php`,
    ensure all composer packages are properly set up
  + The app might complain about `APP_KEY`, run `php artisan key:generate` to generate your app-key which will be automatically written on the `.env` file.

  // Docker & Docker Compose    TBD